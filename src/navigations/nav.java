package navigations;

import org.openqa.selenium.chrome.ChromeDriver;

public class nav {

	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Desktop\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		
		// way 1
		driver.get("http://www.google.com");
		
		// way 2
		driver.navigate().to("http://www.flipkart.com");
		
		driver.navigate().back();
		driver.navigate().forward();
		driver.navigate().refresh();
	}

}
