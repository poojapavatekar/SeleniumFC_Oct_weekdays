package hover;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ex {

	
	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.navigate().to("http://www.amazon.in/");

		Thread.sleep(5000);
		
		WebElement categoryMenu=driver.findElement(By.xpath("//*[@id='nav-link-shopall']/span[2]"));
		
		//hover mouse
		Actions act= new Actions(driver);
		act.moveToElement(categoryMenu).build().perform(); // hovers the mouse on the elem
		
		//wait for list to display
		Thread.sleep(5000);

		// identify the link to be clicked on
		WebElement books=driver.findElement(By.xpath("//*[@id='nav-flyout-shopAll']/div[2]/span[2]/span[1]"));
	
		// click on the link u want to on the list
		act.moveToElement(books).build().perform();
	
		Thread.sleep(3000);
	driver.findElement(By.xpath("//*[@id='nav-flyout-shopAll']/div[3]/div[2]/a[1]/span[1]")).click();
		//driver.close();
	}

}
